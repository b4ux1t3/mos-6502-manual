export type Mnemonic = 
    'ADC'
    | 'AND'
    | 'ASL'
    | 'BCC'
    | 'BCS'
    | 'BEQ'
    | 'BIT'
    | 'BMI'
    | 'BNE'
    | 'BPL'
    | 'BRK'
    | 'BVC'
    | 'BVS'
    | 'CLC'
    | 'CLD'
    | 'CLI'
    | 'CLV'
    | 'CMP'
    | 'CPX'
    | 'CPY'
    | 'DEC'
    | 'DEX'
    | 'DEY'
    | 'EOR'
    | 'INC'
    | 'INX'
    | 'INY'
    | 'JMP'
    | 'JSR'
    | 'LDA'
    | 'LDX'
    | 'LDY'
    | 'LSR'
    | 'NOP'
    | 'ORA'
    | 'PHA'
    | 'PHP'
    | 'PLA'
    | 'PLP'
    | 'ROL'
    | 'ROR'
    | 'RTI'
    | 'RTS'
    | 'SBC'
    | 'SEC'
    | 'SED'
    | 'SEI'
    | 'STA'
    | 'STX'
    | 'STY'
    | 'TAX'
    | 'TAY'
    | 'TSX'
    | 'TXA'
    | 'TXS'
    | 'TYA'


export type AddressingMode = 
    'Immediate'
    | 'Zero Page'
    | 'Zero Page X'
    | 'Absolute'
    | 'Absolute X'
    | 'Absolute Y'
    | 'Indirect X'
    | 'Indirect Y'

export type HexNumber = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'A' | 'B' | 'C' | 'D' | 'E' | 'F' 

export type HexByte = {
    Low: HexNumber,
    High: HexNumber
}

export type OpCode = {
    Code: HexByte,
    ByteSize: number,
    CycleCount: number,
    AddressingMode: AddressingMode
}

export type Instruction = {
    Mnemonic: Mnemonic,
    OpCodes: OpCode[]
}